I had family in town (until Tuesday), so the first opportunity to look at the project was Monday evening. I spent about an hour on the project on Monday night, about 45 mins on Tuesday night, and about an hour on Wednesday night to wrap things up.

Overview of getting started:
- Going through the README (a few times)
- Getting the project setup locally
- Playing with the app
- Studying the coding issues to make sure I understood them
- Reading/skimming the links in the README
- Looking at the code
  - This was my first exposure to Go

## Problem 1 Notes

I'm familiar writing, reading and debugging tests (although not in Go). It looks like the first test failure was due to a capitalization mismatch - ok vs OK. I simply searched for the (uppercase) string "OK", and updated it to lowercase, and that fixed the test. Later on, I also noticed that after this change, in the developer tools console it logged "backend is healthy".

## Problem 2 Notes

The tip about considering how the backend and frontend are interacting helped. I reviewed the code, played with the app and added some `console.log` statements, and eventually noticed the UUID isn't being tested for. The frontend needs this to display/style the messages appropriately (depending on if it's your message or somebody else's).

Off and on I tried to update the tests to account for the UUID, but I just don't have enough familiarly with Go, and ran out of time. From my experience with other languages and testing frameworks though, it seems like it would be a rather simple update, but sadly I couldn't find the right way to do it in Go.

## Problem 3 Notes

I'm definitely more familiar with JS and Vue.js, so this one wasn't too bad. I basically just did a rough draft to prove things out. A test would have been nice, but there weren't any JS ones, and I couldn't figure out the Go ones. Normally I would add tests. I think extracting the messages into a new component would have been nice too.

## Bonus Point 1 Notes

I didn't have time to prove anything out, but if I had more time I would look at XSS vulnerabilities and spoofing UUIDs, both of those things came to mind.
